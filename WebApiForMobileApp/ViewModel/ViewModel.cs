﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiForMobileApp.ViewModel
{
    public class ViewModel
    {
        public string City { get; set; }
        public string UserId { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string PeselNumber { get; set; }
        public string PlayerId { get; set; }

    }
}