﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using Data;
using Data.Models;

namespace WebApiForMobileApp.Controllers
{
    [EnableCors("*", "*", "*")]
    public class WeatherController : ApiController
    {
        //private MobileAppEntities entities = new MobileAppEntities();

        [HttpPost]
        public IHttpActionResult GetWeatherByCity(ViewModel.ViewModel viewModel)
        {
            string url = "http://api.openweathermap.org/data/2.5/weather?q=" + viewModel.City +
                "&appid=85888ecece44b4ec2b84e6d4d544edb8&lang=pl&units=metric";

            WebClient client = new WebClient();

            client.Encoding = System.Text.Encoding.UTF8;
            try
            {
                var json = client.DownloadString(url);
                JObject joResponse = JObject.Parse(json);
                return Ok(joResponse);
            }



            catch (Exception e)
            {
                return NotFound();
            }
        }



        [HttpPost]
        public IHttpActionResult AutoRegisterPlayerId(ViewModel.ViewModel viewModel)
        {

            //var checkPlayerId =
            //    entities.Users.Where(x => x.Player == viewModel.PlayerId).Select(x => x).SingleOrDefault();

            //if (checkPlayerId == null)
            //{

            //    var user = new User
            //    {
            //        Player = viewModel.PlayerId
            //    };
            //    entities.Users.Add(user);

            //    entities.SaveChanges();
            //}

            return Ok();
        }



        [HttpPost]
        public IHttpActionResult RegisterUser(ViewModel.ViewModel viewModel)
        {
            //var checkPlayerId =
            //    entities.Users.Where(x => x.Player == viewModel.PlayerId).Select(x => x).SingleOrDefault();

            //checkPlayerId.Name = viewModel.Name;
            //checkPlayerId.LastName = viewModel.LastName;
            //checkPlayerId.PeselNumber = viewModel.PeselNumber;


            //entities.Entry(checkPlayerId).State =
            //       EntityState.Modified;
          
            //entities.SaveChanges();


            return Ok();

        }


    }
}

