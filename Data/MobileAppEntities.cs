﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;
using MySql.Data.Entity;


namespace Data
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class MobileAppEntities : DbContext
    {
        public MobileAppEntities() : base("server=127.0.0.1;Port=3306;database=MobileAppDb;uid=root;password=rekord")
        {
//server=2c2244af-78ba-48c0-a9b4-a72a0091861d.mysql.sequelizer.com;database=db2c2244af78ba48c0a9b4a72a0091861d;uid=zdyraiyrstcenjaf;pwd=8eTGbMQGDnojKToHTgMqtMGEy3RqCMJqUNdCL5EryzmWfXe7EFyzQS3tEoih4zdY
            {
//
                this.Configuration.ProxyCreationEnabled = false;
            }
        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}
